<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="mirror-subpages-title">
			<span class="text1">ポケット<br />ミラー</span>
			<span class="text2">Pocket Mirror</span>			
		</h2> 	 
		
		<div class="pen-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/mirror_slide_img1.jpg" alt="ポケットミラー" class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<ul>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/mirror_slide_img1.jpg" alt="ポケットミラー" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/mirror_slide_img2.jpg" alt="ポケットミラー" class="thumb" name="mainImage" /></li>					
				</ul>				
			</div>
		</div><!-- .pen-top-left -->		
		
		<div class="pen-top-right clearfix">
			<h3 class="pen-ttable-title">仕様について</h3>
			<table class="pen-ttable">				
				<tr>
					<th>商品名</th>
					<td>ポケットミラー</td>					
				</tr>
				<tr>
					<th>サイズ</th>
					<td>54×5×85mm</td>					
				</tr>
				<tr>
					<th>重量</th>
					<td>約28g</td>					
				</tr>
				<tr>
					<th>材質</th>
					<td>PS、ガラス</td>					
				</tr>
				<tr>
					<th>カラー</th>
					<td>ホワイト、ピンク</td>					
				</tr>
				<tr>
					<th>梱包</th>
					<td>OPP袋：無料</td>					
				</tr>
				<tr>
					<th>ロット</th>
					<td>最低ロット：300個</td>					
				</tr>
				<tr>
					<th>納期</th>
					<td>通常納期：2週間～<br />
					最短納期：5営業日<br />
					（データ校了後発送までの目安です。）
					</td>					
				</tr>
				<tr>
					<th>版代</th>
					<td>パッド印刷：3,000円/1色<br />
					シルク印刷：8,000円/1色</td>					
				</tr>
				<tr>
					<th>備考</th>
					<td>※名入れ専用商品となります。</td>					
				</tr>
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->	

		<div class="pen-top-left mt20 clearfix">
			<p>ポケットサイズのため、必要なときにサッと出して使えます。スタンド式なので使いやすく、小さくて軽いので邪魔にならず、持ち歩ける手軽さが人気のミラーです。発色の良い印刷で、印象に残る販促グッツの作成が可能です。ロゴなどもきれいに印刷できるのでショップのノベルティグッズとしてもおすすめです。</p>
		</div><!-- .pen-top-left -->	
		
		<div class="pen-top-right mt20 clearfix">
			<h3 class="pen-ttable-title">名入れついて</h3>
			<table class="pen-ttable">				
				<tr>
					<th>名入れの<br />サイズ</th>
					<td>四方余白10ｍｍ以外<br />印刷可</td>					
				</tr>	
				<tr>
					<th>色数</th>
					<td>パッド印刷：1色、2色<br />
					シルク印刷：1色（広範囲）<br />
					フルカラー</td>					
				</tr>					
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="pen-estimate-btn">
			<a href="<?php bloginfo('url'); ?>/mirror#estimate-mirror"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="ポケットミラー"></a>
		</div>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ポケットミラー／参考価格例</h3>
		<p class="pen-content1-text">
			【料金表について】<br />
			・表示価格は税抜き価格となります。<br />
			・表示価格は全商品印刷代込の価格となります。
		</p>
		<table class="pen-content1-table">				
			<tr>
				<th></th>			
				<th>300個</th>			
				<th>1,000個</th>			
				<th>3,000個</th>			
				<th>5,000個</th>			
			</tr>	
			<tr>
				<th>1色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th class="second">2色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
		</table><!-- .pen-content1-table -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ポケットミラー制作のおすすめポイント！</h3>
		<div class="pen-content2 clearfix">
			<div class="pen-con2-left mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 1</div>
					<h4 class="pen-con2-title"><span>立体物への印刷</span>や<span>細かい文字</span>などが<span>きれいに印刷</span>できます！</h4>
				</div>
				<div class="pen-con2-text">
					<p>小さな文字や細かな絵柄を高い再現力を発揮します。<br />
					シルク印刷同様ゴールドもシルバーも一般インクと同料金にてご提供いたします。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 2</div>
					<h4 class="pen-con2-title"><span>名入れの範囲が小さい</span>ので<span>版代も低価格でお得</span>です！</h4>
				</div>
				<div class="pen-con2-text">
					<p>比較的安価な版代なのでノベルティグッズとして提供しやすい商品となっています。</p>
				</div>
			</div><!--.pen-con2-right -->
			
			<div class="pen-con2-left clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 3</div>
					<h4 class="pen-con2-title"><span>手頃な価格</span>で<span>オリジナルグッズ制作</span>ができます！</h4>
				</div>
				<div class="pen-con2-text">
					<p>比較的費用が安い印刷方法ですので、予算が決まっている場合のノベルティ作成ではパッド印刷がおすすめです。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 4</div>
					<h4 class="pen-con2-title"><span>小ロット・短納期のご要望</span>にもお応えします！</h4>
				</div>
				<div class="pen-con2-text">
					<p>最短絵使用日13翌日発送の、ロット数500本からのオーダーが可能です。</p>
				</div>
			</div><!--.pen-con2-right -->

		</div><!-- .pen-content2 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','topwork'); ?>	
	
	<div id="estimate-mirror" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="pen-estimate-title"><i class="fa fa-calculator"></i>お見積りフォーム [ポケットミラー]</h3>
		<?php echo do_shortcode('[contact-form-7 id="108" title="estimate_mirror"]') ?>						
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>