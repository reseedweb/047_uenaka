	<h2 class="side-nav-title">ご利用ガイド</h2>
    <ul class="sideNav">
        <li><a href="<?php bloginfo('url'); ?>">トップページ</a></li>
		<li><a href="<?php bloginfo('url'); ?>/guide">はじめての方へ</a></li>
		<li><a href="<?php bloginfo('url'); ?>/price">参考価格例</a></li>
		<li><a href="<?php bloginfo('url'); ?>/draft">データ入稿について</a></li>
        <li><a href="<?php bloginfo('url'); ?>/faq">よくある質問</a></li>
		<li><a href="<?php bloginfo('url'); ?>/company">会社概要</a></li>
		<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>
		<li><a href="<?php bloginfo('url'); ?>/estimate">お見積り</a></li>
		<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
    </ul>
