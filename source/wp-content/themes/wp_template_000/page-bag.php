<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="bag-subpages-title">
			<span class="text1">キャンバス<br />トート</span>
			<span class="text2">Canvas Tote</span>			
		</h2> 	 
		
		<div class="pen-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/bag_slide_img1.jpg" alt="      " class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<ul>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/bag_slide_img1.jpg" alt="キャンバストート" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/bag_slide_img2.jpg" alt="キャンバストート" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/bag_slide_img3.jpg" alt="キャンバストート" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/bag_slide_img4.jpg" alt="キャンバストート" class="thumb" name="mainImage" /></li>
				</ul>				
			</div>
		</div><!-- .pen-top-left -->		
		
		<div class="pen-top-right clearfix">
			<h3 class="pen-ttable-title">仕様について</h3>
			<table class="pen-ttable">				
				<tr>
					<th>商品名</th>
					<td>キャンバストート横型<br />中ナチュラル</td>					
				</tr>
				<tr>
					<th>サイズ</th>
					<td>W300×H210×D100mm（持ち手含まず）<br />約W25×全長290mm（持ち手）</td>					
				</tr>
				<tr>
					<th>材質</th>
					<td>コットン</td>					
				</tr>			
				<tr>
					<th>梱包</th>
					<td>OPP袋：無料</td>					
				</tr>
				<tr>
					<th>ロット</th>
					<td>最低ロット：200枚</td>					
				</tr>
				<tr>
					<th>納期</th>
					<td>通常納期：2週間～<br />
					最短納期：5営業日<br />
					(データ校了発送までの期間目安です)
					</td>					
				</tr>
				<tr>
					<th>版代</th>
					<td>シルク印刷：8,000円/1色</td>					
				</tr>
				<tr>
					<th>備考</th>
					<td>※名入れ専用商品となります。</td>					
				</tr>
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->

		<div class="pen-top-left mt20 clearfix">
			<p>キャンパストートは軽く、持ち運びにも便利で形状によって様々な場面でご使用いただけます。単色、特色から多色印刷に対応できるのがパッド印刷.COMのシルク印刷技術です。単色など色数の少ない印刷が得意で、印象に残るノベルティが制作可能です。ブランドロゴなども綺麗に印刷できるので、ノベルティグッズや、ショップ袋としてもおすすめです。</p>
		</div><!-- .pen-top-left -->		
		
		<div class="pen-top-right mt20 clearfix">
			<h3 class="pen-ttable-title">名入れついて</h3>
			<table class="pen-ttable">				
				<tr>
					<th>名入れの<br />サイズ</th>
					<td>印刷小：W180×H130mm<br />
					印刷大：W230×H80mm</td>					
				</tr>	
				<tr>
					<th>色数</th>
					<td>シルク印刷：1色（広範囲）<br />
					フルカラー</td>					
				</tr>					
			</table>
		</div><!-- .pen-top-right -->	
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="pen-estimate-btn">
			<a href="<?php bloginfo('url'); ?>/bag#estimate-bag"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="キャンバストート"></a>
		</div>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">キャンバストート／参考価格例</h3>
		<p class="pen-content1-text">
			【料金表について】<br />
			・表示価格は税抜き価格となります。<br />
			・表示価格は全商品印刷代込の価格となります。
		</p>
		<table class="pen-content1-table">				
			<tr>
				<th></th>			
				<th>100個</th>			
				<th>1,000個</th>			
				<th>3,000個</th>			
				<th>5,000個</th>			
			</tr>	
			<tr>
				<th>1色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th class="second">2色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
		</table><!-- .pen-content1-table -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">キャンバストート制作のおすすめポイント！</h3>
		<div class="pen-content2 clearfix">
			<div class="pen-con2-left mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 1</div>
					<h4 class="pen-con2-title"><span>安価</span>で<span>きれい</span>な仕上がりです！</h4>
				</div>
				<div class="pen-con2-text14">
					<p>単色など色数の少ない印刷が得意で、色数が少ない場合、安価で綺麗な仕上がりが実現可能となります。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 2</div>
					<h4 class="pen-con2-title"><span>小ロット・短納期のご要望</span>にもお応えします！</h4>
				</div>
				<div class="pen-con2-text14">
					<p>最短営業日13翌日発送の、ロット数100個からのオーダーが可能です。</p>
				</div>
			</div><!--.pen-con2-right -->
			
			<div class="pen-con2-left clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 3</div>
					<h4 class="pen-con2-title">フルカラーも対応で<span>写真も印刷</span>できます</h4>
				</div>
				<div class="pen-con2-text14">
					<p>比較的費用が安い印刷方法ですので、予算が決まっている場合のノベルティ作成ではパッド印刷がおすすめです。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 4</div>
					<h4 class="pen-con2-title"><span>多品種</span>のご要望にもお応えします！</h4>
				</div>
				<div class="pen-con2-text14">
					<p>大きさ・素材、様々な種類のエコバックを取り揃えております。また、ご希望のサイズでの制作も可能です。</p>
				</div>
			</div><!--.pen-con2-right -->

		</div><!-- .pen-content2 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','topwork'); ?>	
	
	<div id="estimate-bag" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="pen-estimate-title"><i class="fa fa-calculator"></i>お見積りフォーム [キャンバストート]</h3>
		<?php echo do_shortcode('[contact-form-7 id="116" title="estimate_bag"]') ?>						
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>