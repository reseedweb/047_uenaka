<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="support-subpages-title">
			<span class="text1">企画・<br />代理店様サポート</span>
			<span class="text2">Support Pack</span>			
		</h2> 		
		<h3 class="h3-title">パッド印刷グッズ制作をお考えの担当者様へ</h3>
		<p class="pl20">弊社はパッド印刷グッズの製作を中心に、多くのイベント会社様・販促会社様・SP広告代理店様とお取引きを頂いております。その理由は、自社工場&国内生産に基づいた確かな品質と、小ロット・短納期などのご要望への柔軟な対応にございます。</p>
		<div class="support-status">例えばこんなお悩みを解決します！</div>
		<div class="support-content">
			<div class="support-info-top clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point1.png" alt="企画・代理店様サポート" />			
				</div>
				<h4 class="support-right">
					プレゼン、提案で使えるサンプルが欲しい	
				</h4>
			</div><!-- .support-info-top -->
			
			<div class="support-info-end clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point2.png" alt="企画・代理店様サポート" />			
				</div>
				<div class="support-right">
					完成品と同じ仕様のサンプルを１個から制作できます！<br />
					（サンプル制作には１週間程いただきますので、納期にご注意ください）
				</div>
			</div><!-- .support-info-end -->
		</div><!-- .support-content -->
		
		<div class="support-content">
			<div class="support-info-top clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point1.png" alt="企画・代理店様サポート" />			
				</div>
				<h4 class="support-right">
					クライアント様の予算内に抑えたい	
				</h4>
			</div><!-- .support-info-top -->
			
			<div class="support-info-end clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point2.png" alt="企画・代理店様サポート" />			
				</div>
				<div class="support-right">
					差し支えなければ、お見積もり時にご予算をお伝えください。予算内に収まるご提案を差し上げます！代理店様向けの価格でお見積もりをお出しいたします。
				</div>
			</div><!-- .support-info-end -->
		</div><!-- .support-content -->
		
		<div class="support-content">
			<div class="support-info-top clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point1.png" alt="企画・代理店様サポート" />			
				</div>
				<h4 class="support-right">
					グッズの仕様について悩んでいる	
				</h4>
			</div><!-- .support-info-top -->
			
			<div class="support-info-end clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point2.png" alt="企画・代理店様サポート" />			
				</div>
				<div class="support-right">
					グッズの仕様についてお悩みの担当者様、用途やご予算・ご要望などをお聞かせください。パッド印刷を携わるメーカーの目線から、ご提案をさせていただきます。
				</div>
			</div><!-- .support-info-end -->
		</div><!-- .support-content -->
		
		<div class="support-content">
			<div class="support-info-top clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point1.png" alt="企画・代理店様サポート" />			
				</div>
				<h4 class="support-right">
					パッド印刷の知識がない	
				</h4>
			</div><!-- .support-info-top -->
			
			<div class="support-info-end clearfix">
				<div class="support-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/support_point2.png" alt="企画・代理店様サポート" />			
				</div>
				<div class="support-right">
					デザイン会社様や印刷会社様の中でも、「パッド印刷のグッズは初めて」という方も多くいらっしゃいます。知識豊富な専門スタッフがサポートしますので、ご安心ください。
				</div>
			</div><!-- .support-info-end -->
		</div><!-- .support-content -->
		
	</div><!-- end primary-row -->	
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>