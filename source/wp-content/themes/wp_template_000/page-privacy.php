<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="privacy-subpages-title">
			<span class="text1">プライバシーポリシー</span>
			<span class="text2">Privacy Policy</span>			
		</h2>		
	</div><!-- end primary-row -->
	
	<div class="privacy-content clearfix">
		<h3 class="h3-title">１．法令及びその他の規範の遵守</h3>
		<p class="ln18em">ご利用になるお客様のプライバシー及び個人情報の保護にあたり、適用 される法令及びその他の規範を遵守いたします。</p>
	</div><!-- .privacy-content -->
	
	<div class="privacy-content clearfix">
		<h3 class="h3-title">２．個人情報の取得・利用</h3>
		<p class="ln18em">ご利用になるお客様から個人情報をご提供いただくときは、提供サービス ごとにその利用目的をあらかじめ明示し適正な方法で取得します。<br />ご提供いただいた個人情報は、明示した利用目的の範囲で利用します。</p>
	</div><!-- .privacy-content -->
	
	<div class="privacy-content clearfix">
		<h3 class="h3-title">３．個人情報の提供</h3>
		<p class="ln18em">ご利用になるお客様からご提供いただきました個人情報を第三者に開示又は 提供いたしません。<br />ただし、法令に基づく場合など、正当な理由がある場合を 除きます。</p>
	</div><!-- .privacy-content -->
	
	<div class="privacy-content clearfix">
		<h3 class="h3-title">４．個人参加・公開</h3>
		<p class="ln18em">保有する個人情報を正確かつ最新の内容に保ちます。 また、ご利用になるお客様本人から私が保有する個人情報の開示、訂正又は削除 を求められたときは、ご本人確認後、速やかにこれに応じます。<br />
		ただし、法令又は規約等により、個人情報の保存期間が定められているときは、 保存期間の経過後に削除します。</p>
	</div><!-- .privacy-content -->
	
	<div class="privacy-content clearfix">
		<h3 class="h3-title">５．安全管理対策</h3>
		<p class="ln18em">収集した個人情報に対し、不正アクセス、紛失、破壊、改ざん及び 漏えい等の予防並びに是正措置を講じ、厳正な管理の下で安全に蓄積・保管します。</p>
	</div><!-- .privacy-content -->
	
	<div class="privacy-content clearfix">
		<h3 class="h3-title">６．継続的改善</h3>
		<p class="ln18em">個人情報保護に関するコンプライアンス・プログラムを継続的に見直し改善を行います。</p>
	</div><!-- .privacy-content -->
		
	<?php get_template_part('part','goods'); ?>	
<?php get_footer(); ?>