<?php get_header(); ?>
	<div id="01" class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="flow-subpages-title">
			<span class="text1">ご注文の流れ</span>
			<span class="text2">Flow</span>			
		</h2>
		<h3 class="h3-title">1.お問い合わせ・ヒアリング</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="ご注文の流れ" />
			</div>
			<div class="text ln18em">
				<p>まずはお気軽にお問い合わせください。<br />
				パッド印刷の仕様のことで迷っている項目や疑問点がございましたら、お気軽にお尋ねください。<br />
				価格のご相談や、当サイトに掲載しているグッズ以外のデザイン制作物のご依頼もお待ちしております。<br />
				基本的に即日見積りを目指しています。<br />
				当サイトに掲載しているグッズ以外の仕様によってはお見積りに時間をいただく場合もございます。ご了承ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div id="02" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">2.お見積り</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="ご注文の流れ" />
			</div>
			<div class="text ln18em">
				<p>打ち合わせ内容でお聞きしたご要望より、パッド印刷グッズのお見積りをお伝えさせていただきます。仕様を迷われている場合は、複数案のお見積りも可能ですので、お気軽にご相談ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div id="03" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">3.ご注文・データ入稿</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="ご注文の流れ" />
			</div>
			<div class="text ln18em">
				<p>決定した仕様に基づいての見積りをご提出します。仕様、見積りに納得いただきましたら、正式にご注文となります。<br />
				パッド印刷のデザインデータをご入稿ください。<br />
				データ形式はAdobe IllustratorのCS4までを推奨しております。Adobe Illustrator以外のデータは別途費用かかる場合がございますので、ご了承ください。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div id="04" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">4.製造</h3>
		<div class="message-left message-308 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="ご注文の流れ" />
			</div>
			<div class="text ln18em">
				<p>「パッド印刷.com」運営の自社工場により製造を行います。製造されたグッズは丁寧に検品が行われ出荷されます。パッド印刷製作の過程につきましては、こちらのページで詳しく掲載しております。よろしければ、ご覧下さい。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<div id="05" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">5.ご納品</h3>
		<div class="message-left message-308 mb50 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="ご注文の流れ" />
			</div>
			<div class="text ln18em">
				<p>全国のお客様へ発送、指定日にあわせて納品いたします。</p>
			</div>
		</div><!-- end message-308 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods');?>	
<?php get_footer(); ?>