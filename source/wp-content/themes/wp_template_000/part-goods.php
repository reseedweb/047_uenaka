<div class="primary-row clearfix"><!-- begin primary-row -->
	<?php if(is_page('estimate')) : ?>																					
	<?php else : ?>
		<h2 class="h2-title">パッド印刷でこんなグッズが制作できます！</h2> 
	<?php endif; ?>	
    <div class="top-content3 clearfix">
		<div class="top-con3-left">
			<h3 class="top-con3-title">今治ミニタオルハンカチ</h3>
			<div class="top-con3-bg">	
				<div class="top-con3-info clearfix">
					<div class="top-subcon3-left"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_img1.jpg" alt="<?php bloginfo('name');?>" /></div>
					<div class="top-subcon3-right">
						<p class="top-subcon3-text">実用的で子供から大人まで男女を問わず喜ばれるアイテムですので企業・店舗のノベルティやイベントグッズとしても多用いただけます。</p>
						<p class="top-subcon3-price"><span><img src="<?php bloginfo('template_url'); ?>/img/top/top_con3_icon.jpg" alt="<?php bloginfo('name');?>" /></span>&yen;158～</p>
					</div>
				</div>				
				<p class="top-subcon3-btn"><a href="<?php bloginfo('url'); ?>/towel"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>	
			</div>			
		</div><!-- .top-con3-left -->

		<div class="top-con3-right">
			<h3 class="top-con3-title">ポケットミラー</h3>
			<div class="top-con3-bg">	
				<div class="top-con3-info clearfix">
					<div class="top-subcon3-left"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_img2.jpg" alt="<?php bloginfo('name');?>" /></div>
					<div class="top-subcon3-right">
						<p class="top-subcon3-text">ポケットにも入るサイズのコンパクトミラーは実用性が高いので、企業・店舗のノベルティやイベントグッズとしても多用いただけます。</p>
						<p class="top-subcon3-price"><span><img src="<?php bloginfo('template_url'); ?>/img/top/top_con3_icon.jpg" alt="<?php bloginfo('name');?>" /></span>&yen;35～</p>
					</div>
				</div>				
				<p class="top-subcon3-btn"><a href="<?php bloginfo('url'); ?>/mirror"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>	
			</div>			
		</div><!-- .top-con3-right -->
	</div><!-- .top-content3 -->
	
	<div class="top-content3 clearfix">
		<div class="top-con3-left">
			<h3 class="top-con3-title">ボールペン</h3>
			<div class="top-con3-bg">	
				<div class="top-con3-info clearfix">
					<div class="top-subcon3-left"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_img3.jpg" alt="<?php bloginfo('name');?>" /></div>
					<div class="top-subcon3-right">
						<p class="top-subcon3-text">会社の社名やロゴを印刷できます。実用性が高いので、企業・店舗のノベルティやイベントグッズとしても多用いただけます。手頃な価格で制作でき、大変人気な商材です。</p>
						<p class="top-subcon3-price"><span><img src="<?php bloginfo('template_url'); ?>/img/top/top_con3_icon.jpg" alt="<?php bloginfo('name');?>" /></span>&yen;50～</p>
					</div>
				</div>				
				<p class="top-subcon3-btn"><a href="<?php bloginfo('url'); ?>/pen"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>	
			</div>			
		</div><!-- .top-con3-left -->

		<div class="top-con3-right">
			<h3 class="top-con3-title">キャンパストート</h3>
			<div class="top-con3-bg">	
				<div class="top-con3-info clearfix">
					<div class="top-subcon3-left"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_img4.jpg" alt="<?php bloginfo('name');?>" /></div>
					<div class="top-subcon3-right">
						<p class="top-subcon3-text">ランチバッグとしても、ちょっとしたお出かけにも使い勝手のよいキャンバストートです。</p>
						<p class="top-subcon3-lastprice"><span><img src="<?php bloginfo('template_url'); ?>/img/top/top_con3_icon.jpg" alt="<?php bloginfo('name');?>" /></span>&yen;450～</p>
					</div>
				</div>				
				<p class="top-subcon3-btn"><a href="<?php bloginfo('url'); ?>/bag"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>	
			</div>			
		</div><!-- .top-con3-right -->
	</div><!-- .top-content3 -->
		
	
	<div class="top-content4 clearfix">
		<h3 class="top-con4-title">その他・パッド印刷グッズ</h3>
		<div class="top-con4-info clearfix">
			<div class="top-con4-left"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content4_img.jpg" alt="<?php bloginfo('name');?>" /></div>
			<div class="top-con4-right">	
				<h3 class="top-con4-rtitle">お持ち込みOK！商材仕入れも代行いたします！お気軽にご相談を。</h3>
				<p>パッド印刷グッズなどその他商品へ印刷も可能！<br />その他商品に関しましても、直接当社へ商品を持ち込みいただければ、ご相談いたします！またお客様から商材のご指定をいただければ、商材の仕入れも代行しております。</p>
				<p class="top-con4-btn"><a href="<?php bloginfo('url'); ?>/othergoods"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content3_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>	
			</div>
		</div>
	</div>
</div><!-- end primary-row -->