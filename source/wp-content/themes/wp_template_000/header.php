<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
		<script src="<?php bloginfo('template_url'); ?>/js/jquery.reseed.js" type="text/javascript"></script>		
		<script src="<?php bloginfo('template_url'); ?>/js/tgImageChangeV2.js" type="text/javascript"></script>		
		<script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
		<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>   
		<link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />        
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/slick.css"/>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/slick-theme.css"/>			
		<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick.min.js"></script>		
        <?php wp_head(); ?>
    </head>
	
    <body>     
        <div id="screen_type"></div>        
        <div id="wrapper"><!-- begin wrapper -->		
            <section id="top"><!-- begin top -->                
                <div class="contain clearfix">
                    <h1 class="top-text">パッド印刷・シルク印刷のグッズ制作・印刷なら</h1>
                    <ul class="top-navi">
                        <li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
						<li><a href="<?php bloginfo('url'); ?>/company">会社概要</a></li>
						<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>                                             
                    </ul>					
                </div>                
            </section><!-- end top -->            

            <header><!-- begin header -->
                <div class="contain clearfix">
                    <div class="header-content clearfix">            
                        <div class="logo">
                            <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
                        <div class="header-tel">
                            <img src="<?php bloginfo('template_url'); ?>/img/common/header_tel.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
						<div class="header-contact">
                            <a href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/common/header_contact.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
						<div class="header-estimate">
                            <a href="<?php bloginfo('url'); ?>/estimate"><img src="<?php bloginfo('template_url'); ?>/img/common/header_estimate.jpg" alt="<?php bloginfo('name');?>" /></a>
                        </div>
                    </div>                
                </div>
            </header><!-- end header -->
            
			<?php get_template_part('part','navi'); ?>
			
			<?php if (!is_home()) : ?>
               <?php get_template_part('part','breadcrumb'); ?>				
			<?php endif;?>	

            			
            <section id="content"><!-- begin content -->
                <div class="contain two-cols-left clearfix"><!-- begin two-cols -->
                    <main class="primary"><!-- begin primary -->
       