			<section id="top-flow_contact">
				<div class="contain clearfix">
					<h2 class="top-fc-title">ご注文の流れ</h2>
					<div class="top-fc-content clearfix">						
						<ul class="top-fc-flow">
							<li>
								<a href="<?php bloginfo('url'); ?>/flow#01">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_flow_img1.jpg" alt="<?php bloginfo('name');?>" />
									<span>お問い合わせ<br />ヒアリング</span>
								</a>
							</li>
							<li>
								<a href="<?php bloginfo('url'); ?>/flow#02">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_flow_img2.jpg" alt="<?php bloginfo('name');?>" />
									<span>お見積り</span>
								</a>
							</li>
							<li>
								<a href="<?php bloginfo('url'); ?>/flow#03">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_flow_img3.jpg" alt="<?php bloginfo('name');?>" />
									<span>ご注文<br />データ入稿<span>
								</a>
							</li>
							<li>
								<a href="<?php bloginfo('url'); ?>/flow#04">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_flow_img4.jpg" alt="<?php bloginfo('name');?>" />
									<span>製造</span>
								</a>
							</li>
							<li>
								<a href="<?php bloginfo('url'); ?>/flow#05">
									<img src="<?php bloginfo('template_url'); ?>/img/top/top_flow_img5.jpg" alt="<?php bloginfo('name');?>" />
									<span>ご納品</span>
								</a>
							</li>
						</ul><!-- .top-fc-flow -->
						<p class="top-fc-contact">
							<a class="top-fc-conbtn" href="<?php bloginfo('url'); ?>/contact"><img src="<?php bloginfo('template_url'); ?>/img/top/top_contact_img1.jpg" alt="<?php bloginfo('name');?>" /></a>
							<a class="top-fc-estbtn" href="<?php bloginfo('url'); ?>/estimate"><img src="<?php bloginfo('template_url'); ?>/img/top/top_contact_img2.jpg" alt="<?php bloginfo('name');?>" /></a>
						</p><!-- .top-fc-contact -->
					</div><!-- .top-fc-content -->
				</div>
			</section><!-- .top-flow_contact -->