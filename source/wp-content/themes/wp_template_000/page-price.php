<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="price-subpages-title">
			<span class="text1">参考価格例</span>
			<span class="text2">Reference Price</span>			
		</h2>
		<h3 class="h3-title">商材別参考価格例 一覧</h3>
		<p class="ln18em">下記基本仕様でオリジナルグッズを制作した場合の、参考の単価を提示しております。<br />
		※あくまで参考の価格例になります。商品の仕様、印刷によって価格は変わってまいりますので、詳しいお見積もりは各商材ページのお見積もりフォーム、もしくお電話よりお問いあわせ下さい。コスト削減のご相談もお待ちしております！</p>
		<ul class="price-link clearfix">
			<li><a href="<?php bloginfo('url'); ?>/price#01">ミニタオルハンカチ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/price#02">ポケットミラー</a></li>
			<li><a href="<?php bloginfo('url'); ?>/price#03">ボールペン</a></li>
			<li><a href="<?php bloginfo('url'); ?>/price#04">トートバッグ</a></li>
		</ul>
	</div><!-- end primary-row -->
	
	
	<div id="01" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">ミニタオルハンカチ</h3>
		<div class="message-left message-200 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img1.jpg" alt="参考価格例" />
			</div>
			<div class="text ln18em">
				<p>吸水性と肌触りにこだわり、厳しい審査をクリアした確かな品質の今治タオルミニハンカチです。普段の生活にかかせない商品は、もらって嬉しいノベルティグッズです。実用的で子供から大人まで男女を問わず喜ばれるアイテムですので企業・店舗のノベルティやイベントグッズとしても多用いただけます。<br />
				<span>シルク印刷：版代8.000円<span></p>
			</div>
		</div><!-- end message-200 -->
		<h4 class="price-subtitle">参考価格例</h4>
		<table class="price-table">				
			<tr>
				<th></th>			
				<th>200枚</th>			
				<th>1,000枚</th>									
				<th>3,000枚</th>		
				<th>5,000枚</th>		
			</tr>	
			<tr>
				<th>1色（シルク）</th>
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>
			</tr>
		</table><!-- price-table -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="pen-estimate-btn">
				<a href="<?php bloginfo('url'); ?>/pen#estimate-pen"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="参考価格例"></a>
			</div>
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	
	<div id="02" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">ポケットミラー</h3>
		<div class="message-left message-200 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img2.jpg" alt="参考価格例" />
			</div>
			<div class="text ln18em">
				<p>ポケットサイズのため、必要なときにサッと出して使えます。スタンド式なので使いやすく、小さくて軽いので邪魔にならず、持ち歩ける手軽さが人気のミラーです。発色の良い印刷で、印象に残る販促グッツの作成が可能です。ロゴなどもきれいに印刷できるのでショップのノベルティグッズとしてもおすすめです。<br/>
				<span>パッド印刷：版代3.000円</span><br />
				<span>シルク印刷：版代8.000円</span></p>
			</div>
		</div><!-- end message-200 -->
		<h4 class="price-subtitle">参考価格例</h4>
		<table class="price-table">				
			<tr>
				<th></th>			
				<th>300個</th>			
				<th>1,000個</th>									
				<th>3,000個</th>		
				<th>5,000個</th>		
			</tr>	
			<tr>
				<th>1色（パッド）</th>
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>
			</tr>
			<tr>
				<th>2色（パッド）</th>
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>
			</tr>
			<tr>
				<th>1色（パッド）</th>
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>
			</tr>
		</table><!-- price-table -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="pen-estimate-btn">
				<a href="<?php bloginfo('url'); ?>/pen#estimate-pen"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="参考価格例"></a>
			</div>
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	
	<div id="03" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">ボールペン</h3>
		<div class="message-left message-200 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img3.jpg" alt="参考価格例" />
			</div>
			<div class="text ln18em">
				<p>企業様のノベルティグッズやオリジナルの記念品などに定番の、参考価格例です。インクから組み立てまで全て日本で製造されており、高品質で滑らかな書き味です。<br />
				ラバークリップが付いているので長時間の筆記も疲れにくいようになっています。パッド印刷の強みを生かし、立体物への印刷や細かい文字もきれいに印刷できるので、企業様のロゴマークなどきれいに印刷することが可能です。<br />
				さらに手頃な価格で制作できるので、パッド印刷グッズの中でも大変人気な商品です。<br />
				<span>パッド印刷：版代3.000円</span></p>
			</div>
		</div><!-- end message-200 -->
		<h4 class="price-subtitle">参考価格例</h4>
		<table class="price-table">				
			<tr>
				<th></th>			
				<th>500個</th>			
				<th>1,000個</th>									
				<th>3,000個</th>		
				<th>5,000個</th>		
			</tr>	
			<tr>
				<th>1色</th>
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>
			</tr>
			<tr>
				<th>2色</th>
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>
			</tr>
		</table><!-- price-table -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="pen-estimate-btn">
				<a href="<?php bloginfo('url'); ?>/pen#estimate-pen"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="参考価格例"></a>
			</div>
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	
	<div id="04" class="primary-row clearfix"><!-- begin primary-row -->	
		<h3 class="h3-title">トートバッグ</h3>
		<div class="message-left message-200 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/price_content_img4.jpg" alt="参考価格例" />
			</div>
			<div class="text ln18em">
				<p>ランチバッグとしても、ちょっとしたお出かけにも使い勝手のよいキャンバストートは企業のPRにも最適です。キャンバスバッグは、別名、『帆布』とも呼ばれ平織りで織られた厚手の布で、一般的には綿または麻で織られた布地になります。 厚手で丈夫な布、という目的で生産されたもので、帆船の帆に使用されていたために、『帆布（はんぷ）』と呼ばれるようになりました。質感は高く、耐久性に優れ、強固さがキャンバスバッグの特徴です。
				<span>シルク印刷：版代8.000円</span></p>
			</div>
		</div><!-- end message-200 -->
		<h4 class="price-subtitle">参考価格例</h4>
		<table class="price-table">				
			<tr>
				<th></th>			
				<th>100枚</th>			
				<th>1,000枚</th>									
				<th>3,000枚</th>		
				<th>5,000枚</th>		
			</tr>	
			<tr>
				<th>1色（シルク）</th>
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>	
				<td>00円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>	
				<td>000円</td>
			</tr>			
		</table><!-- price-table -->
		
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="pen-estimate-btn">
				<a href="<?php bloginfo('url'); ?>/pen#estimate-pen"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="参考価格例"></a>
			</div>
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods');?>	
<?php get_footer(); ?>