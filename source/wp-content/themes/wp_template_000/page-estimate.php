<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="estimate-subpages-title">
			<span class="text1">お見積り</span>
			<span class="text2">Estimate</span>			
		</h2>
		<h3 class="h3-title">商材別お見積りフォームのご案内</h3>	
		<p class="ln2em">パッド印刷グッズの商材別お見積もりフォームです。<br />
		その他のグッズをご希望の方・まずはご相談したいという方は、当サイトの「お問い合わせ」もしくはお電話（TEL：072-279-8798）にてお気軽にお問い合わせください。</p>
	</div><!-- end primary-row -->
		
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>