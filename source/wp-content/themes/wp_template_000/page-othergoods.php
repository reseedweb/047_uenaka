<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="othergoods-subpages-title">
			<span class="text1">その他・<br />パッド印刷グッズ</span>
			<span class="text2">Other Goods</span>			
		</h2> 	 
		<h3 class="h3-title">パッド印刷でどんなものが作れるの？</h3>
		<div class="othergoods-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/othergoods_slide_img1.jpg" alt="その他・パッド印刷グッズ" class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<ul>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/othergoods_slide_img1.jpg" alt="その他・パッド印刷グッズ" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/othergoods_slide_img2.jpg" alt="その他・パッド印刷グッズ" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/othergoods_slide_img3.jpg" alt="その他・パッド印刷グッズ" class="thumb" name="mainImage" /></li>					
				</ul>				
			</div>
		</div><!-- .othergoods-top-left -->		
		
		<div class="othergoods-top-right clearfix">
			パッド印刷は凹版を使用して版上のインキをシリコンパッドに一次転写し、被印刷物に二次転写を行なうオフセット印刷の一種です。<br />
			弾力のあるシリコンパッドで印刷するため、平面ばかりでなく多少の曲面や凹凸面にも名入れが可能です。<br />
			本サイトの商材ラインアップとして紹介している、「ミニタオルハンカチ」「ポケットミラー」「ボールペン」「トートバッグ」以外にも、パッド印刷で制作できるオリジナルグッズはたくさんあります。<br />
			「作ってみたい商材がある」「予算感や仕様などの詳細が知りたい」など少しでも興味をお持ちの方は、企画段階でのご相談でも構いませんので、お問い合わせフォームやお電話からお気軽にご相談ください。
		</div><!-- .othergoods-top-right -->					
	</div><!-- end primary-row -->
		
	<?php get_template_part('part','topwork'); ?>	
	
	<div id="estimate-othergoods" class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="pen-estimate-title"><i class="fa fa-envelope-o"></i>お問い合わせフォーム</h3>
		<?php echo do_shortcode('[contact-form-7 id="42" title="お問い合わせ"]') ?>						
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>