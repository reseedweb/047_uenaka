                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

			<?php get_template_part('part','flow_contact'); ?>  			
			
			<?php if(is_home()) : ?>																											
				<section id="top-intro">
					<div class="contain clearfix">
						<h2 class="top-intro-title">パッド印刷.comからのご挨拶</h2>
						<div class="top-intro-content clearfix">
							<div class="top-intro-left"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content_intro.jpg" alt="<?php bloginfo('name');?>" /></div>
							<div class="top-intro-right">
								<p>はじめまして、この度は『パッド印刷.com』ご覧いただきありがとうございます。『パッド印刷.com』は大阪でパッド印刷とシルク印刷で製品を印刷・製造している会社です。</p>
								<p>このサイトでは特にパッド印刷.comでのオリジナルグッズ制作を中心に、商材のご案内をしております。</p>
								<p>パッド印刷とは、凹版を使用して版上のインキをシリコンパッドに一次転写し、被印刷物に二次転写を行なうオフセット印刷の一種です。 やわらかいパッドが被印刷物になじむ為、平面ばかりでなく、曲面、凹凸面にも印刷が可能ですので平面だけでなく立体的な商品の印刷に特化しております。</p>
								<p>またインキの乾燥も早いため、ウェットオンウェットでの連続印刷（多色印刷・重ね打ち）にも適しております。</p>
								<p>そんなパッド印刷の強みを生かし、弊社ではボールペンやハンカチ、ハンドミラー、トートバッグなどのオリジナルグッズ制作を承っております。もちろんサイトに掲載されている以外のパッド印刷グッズ制作のご相談も承っており商品仕入れ代行もしております。</p>
								<p>自社工場での制作なので、低価格&高品質、そして小ロット・短納期のご要望にも柔軟に対応いたします。</p>
								<p>企業様のノベルティ・販促グッズ・イベントグッズや、個人様の同人グッズ・ギフト品のオリジナル制作なら、『パッド印刷.com』に是非お任せください！</p>
							</div>
						</div>
					</div>
				</section><!-- .top-intro -->
			<?php endif; ?>		
			
			
			<footer><!-- begin footer -->				
				<div class="footer-content">
					<div class="contain clearfix">
						<div class="footer-con-left">
							<p><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.png" alt="<?php bloginfo('name');?>" /></a></p>
							<p class="footer-tel"><img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.png" alt="<?php bloginfo('name');?>" /></p>
						</div>
						<?php get_template_part('part','footerNav'); ?>
					</div>					
				</div>		
                
                <div class="footer-copyright"><!-- begin wrapper -->
					<p class="contain clearfix">Copyright©2015 パッド印刷.com All Rights Reserved.</p>
                </div>         
            </footer><!-- end footer -->  	            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>              
    </body>
</html>