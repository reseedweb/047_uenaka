<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="faq-subpages-title">
			<span class="text1">よくある質問</span>
			<span class="text2">Faq</span>			
		</h2>
		<h3 class="h3-title">パッド印刷グッズについて</h3>
		<div class="faq-content">
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">パッド印刷で印刷できるものを教えてください</h4>
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>パッド印刷は、シルクスクリーン印刷と比較されることが多いのですが、パッド印刷がシルクスクリーン印刷に対して優れているのは、以下のポイントです。</p>
					<p class="faq-text-bold">1. 三次曲面や凹凸面に印刷可能<br />
					2. 乾燥工程なしに連続多色印刷が可能<br />
					3. 小文字や細い線の印刷に優れている<br />
					4. インキ使用量が少ない<br />
					5. 印刷スピードが上げられる</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->
		
		<div class="faq-content">
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">サイズや形状など完全オリジナルの商品を制作したい</h4>				
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>商材によってはサイズ・形状すべてオリジナルで制作できるものがございます。（その際、別途型代や加工代金がかかる場合がございます。）<br />
					当サイト掲載外の既製品・規制サイズで対応できる場合もありますので、まずはお気軽にご相談ください。</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->
		
		<div class="faq-content">
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">成形品持ち込み商品への印刷は可能ですか？</h4>				
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>はい。<br />素材に応じたインキの選定と密着テストを実施しますので、テスト用のパーツがあると助かります。</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->
		
		<div class="faq-content">
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">これまでに制作した実績をみたい</h4>				
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>これまでに弊社で作成した実績サンプルを郵送にてお送りすることが可能です。（在庫によってお送りできない場合もございます。ご了承ください。）もしくは弊社事務所にて制作実績をご覧いただきながら、打ち合わせさせていただくことも可能です。お気軽にご連絡ください。</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->		
	</div><!-- end primary-row -->
		
		
	<div class="primary-row clearfix">
		<h3 class="h3-title">ご注文について</h3>
		<div class="faq-content"><!-- begin faq-content -->
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">サンプル品は制作できますか？</h4>
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>可能です。１つからお作りいただけます。（別途費用）<br />
					サンプル品を制作される場合は、通常納期に加えてさらに１週間ほど納期をいただきます。あらかじめご了承ください。<br />
					実際の納期については、仕様やロット数によって変わって参りますので、弊社スタッフまでお問い合わせください。</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->
		
		<div class="faq-content">
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">商品の個別包装はできますか？</h4>				
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>可能です。<br />
					OPP袋にて個別に包装いたします。<br />
					その他の個別包装は別途料金がかかる場合がございます。</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->
		
		<div class="faq-content clearfix">
			<div class="faq-info-question clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_question.png" alt="よくある質問" />			
				</div>
				<h4 class="faq-right">返品・交換はできますか？</h4>				
			</div><!-- .faq-info-question -->
			
			<div class="faq-info-answer clearfix">
				<div class="faq-left">
					<img src="<?php bloginfo('template_url'); ?>/img/content/faq_answer.png" alt="よくある質問" />			
				</div>
				<div class="faq-right">
					<p>万が一製品に不備があった場合は、直ちに返品・交換対応をさせていただきます。
					ただし、次の場合には返品・交換の対応ができかねます。
					製品到着後１週間以上経っている場合や、お客様都合によるもの、配送時が原因と思われる多少のホコリや印刷面の擦れ、色調の誤差が一定の許容範囲である場合。</p>
				</div>
			</div><!-- .faq-info-answer -->			
		</div><!-- .faq-content -->			
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
<?php get_footer(); ?>