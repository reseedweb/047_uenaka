<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="about-subpages-title">
			<span class="text1">パッド印刷とは？</span>
			<span class="text2">About the Pad Printing </span>			
		</h2>
		<div class="about-content1-top clearfix">
			<span class="about-tcon1-title1">パッド印刷の<br />印刷工程</span>
			<span class="about-tcon1-title2">The Processes<br />of PAD Printing </span>
			<div class="about-tcon1-text">
				<p>【1】印刷したい図案で作成した凹版プレートに、スキージ（ヘラ）Bでインクを流し込む</p>
				<p>【2】凹版プレート上に余ったインクをスキージ（ヘラ）Aが掻き出す（凹部にのみインクが残る）</p>
				<p>【3】シリコンパッドを凹版プレートに押し付け、凹部のインクをパッドに付着させる</p>
				<p>【4】インクが付いたシリコンパッドを商品に押し付け、商品に印刷する</p>
			</div>
		</div>
		<div class="about-content1-text clearfix">
			<p>パッド印刷とは凹版を使用して版上のインキをシリコンパッドに一次転写し、被印刷物に二次転写を行なうオフセット印刷の一種です。弾力のあるシリコンパッドで印刷するため、平面ばかりでなく多少の曲面や凹凸面にも名入れが可能です。</p>
			<p>比較的費用が安い印刷方法ですので、ノベルティなどの作成には、パッド印刷をおすすめさせていただくこともあります。 小さな文字、ロゴマークなども比較的表現しやすい印刷方法です。</p>
		</div>
	</div><!-- end primary-row -->	
	
	
	<div class="mt30 clearfix"><!-- begin primary-row -->
		<div class="about-content2-top clearfix">
			<span class="about-tcon2-title">安心の自社一貫生産</span>
			<p class="about-tcon2-text">パッド印刷.comでは印刷・縫製のほぼ全ての工程を自社工場内で行うことで、品質を気にされるお客様にも満足いただいております。また小ロット・短納期・多品種のご要望にも柔軟に対応させていただきます。</p>			
		</div>
		<div class="about-content2 clearfix">
			<div class="about-con2-left mb40">
				<p class="about-c2l-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img1.jpg" alt="パッド印刷とは" /></p>
				<p class="about-c2l-text">印刷する商品を乗せて位置を調整をします。微調整しながら最終位置を合わせます。</p>
			</div>
			
			<div class="about-con2-right mb40">
				<p class="about-c2l-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img2.jpg" alt="パッド印刷とは" /></p>
				<p class="about-c2l-text">印刷したいデザインをエッチングした凹版にインキを流し込み、余分なインキを搔き出取る。</p>
			</div>
			
			<div class="about-con2-left">
				<p class="about-c2l-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img3.jpg" alt="パッド印刷とは" /></p>
				<p class="about-c2l-text">シリコンパッドを凹版面に押し当て、インキを転移させる。</p>
			</div>
			
			<div class="about-con2-right">
				<p class="about-c2l-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content2_img4.jpg" alt="パッド印刷とは" /></p>
				<p class="about-c2l-text">シリコンパッド表面に転移されたインキを印刷対象物に転写する。</p>
			</div>
		</div><!-- .about-content2 -->
	</div><!-- end primary-row -->	
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">パッド印刷の特徴</h3>
		<p class="pl20">パッド印刷の4つの特徴をご紹介します。</p>
		<div class="message-group message-col365"><!-- begin message-group -->
			<div class="message-row mt30 clearfix"><!--message-row -->
				<div class="message-col about-content3">
					<div class="about-con3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content3_img1.jpg" alt="パッド印刷とは" /></div>
					<div class="about-con3-info">
						<div class="about-con3-point">特徴 1</div>
						<h4 class="about-con3-title">凹部の内側に<br />印刷ができる</h4>
						<p class="about-con3-text">柔らかいシリコンパッドを利用することにより、凹凸のある立体物への印刷が可能になります。</p>
					</div>
				</div><!-- end message-col -->
				
				<div class="message-col about-content3">
					<div class="about-con3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content3_img2.jpg" alt="パッド印刷とは" /></div>
					<div class="about-con3-info">
						<div class="about-con3-point">特徴 2</div>
						<h4 class="about-con3-title">細かい図柄が<br />印刷できる</h4>
						<p class="about-con3-text">ボールペンなどの小さい物にも細かな印刷が可能です。</p>
					</div>
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->
			
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col about-content3">
					<div class="about-con3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content3_img3.jpg" alt="パッド印刷とは" /></div>
					<div class="about-con3-info">
						<div class="about-con3-point">特徴 3</div>
						<h4 class="about-con3-title">多色刷りも<br />できます</h4>
						<p class="about-con3-text">単色刷りが基本ですが　多色刷りの場合はその色数分の重ね打ちになります。</p>
					</div>
				</div><!-- end message-col -->
				
				<div class="message-col about-content3">
					<div class="about-con3-img"><img src="<?php bloginfo('template_url'); ?>/img/content/about_content3_img4.jpg" alt="パッド印刷とは" /></div>
					<div class="about-con3-info">
						<div class="about-con3-point">特徴 4</div>
						<h4 class="about-con3-title">低コストで<br />制作できる</h4>
						<p class="about-con3-text">樹脂版を使用すると、多品種少量の印刷にも低コストで対応できる。</p>
					</div>
				</div><!-- end message-col -->				         
			</div><!-- end message-row -->			
		</div><!-- end message-group -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">パッド印刷とシルク印刷</h3>
		<div class="about-content4 clearfix">
			<h4 class="about-con4-title">パッド印刷</h4>
			<p class="about-con4-text">
				インクをシリコンパッドに転写し、ノベルティにスタンプのように押しつけて印刷します。<br />
				ボールペンやライトなど小さめのノベルティに、小さな文字や細かな絵柄も表現しやすく、シルク印刷同様、よく使われる印刷方式です。シリコンパッドの弾力を活かし、平面ばかりでなく、曲面、凹凸面にも印刷が可能です。
			</p>
			<h4 class="about-con4-title">シルク印刷</h4>
			<p class="about-con4-text">
				シルク印刷は絹（シルク）の布を版材に使った印刷方法。絹目からインクを押しだすため、インク量を多く調節することができます。そのためバッグ等、インクが染み込んでしまう素材へ、力強くかつ広い面積への名入れが可能です。また、ガラス・金属・プラスチックなど多くのノベルティへの名入れに活用されています。
			</p>
			<table class="about-con4-table">				
				<tr>
					<th></th>			
					<th>パッド印刷</th>			
					<th>シルク印刷</th>									
				</tr>	
				<tr>
					<th>価格</th>
					<td>予算が決まっている場合のノベルティ作成ではパッド印刷がおすすめです。</td>	
					<td>単色であれば安価ですが、色数が多いとそのぶん版代がかさみます。</td>				
				</tr>
				<tr>
					<th class="about-con4-clr">納期</th>
					<td>最短4営業日以内発送といった短納期にも対応可能です。</td>	
					<td>版を制作する必要やインクの乾燥に時間がかかるため、昇華プリントより制作期間がかかります。</td>				
				</tr>
				<tr>
					<th>フルカラー印刷</th>
					<td>インク層も薄く乾燥が速いため、印刷直後の重ね刷りもでき、曲面・平面でのフルカラー印刷も可能です。</td>	
					<td>ほとんどの色を再現できますが、グラデーションや色数の多いデザインは苦手です。</td>				
				</tr>
				<tr>
					<th class="about-con4-clr">単色印刷</th>
					<td>網点印刷により、単色でも色の濃淡を表現でき、グラデーションの印刷まで可能です。</td>	
					<td>単色や２色印刷など色数の少ないデザインが大得意で、きれいに安価に仕上がります。</td>				
				</tr>
				<tr>
					<th>印刷可能な素材</th>
					<td>多少の曲面や凹凸面の素材にも印刷が可能です。</td>	
					<td>ほとんどの素材に印刷が可能で、持ち込みの既存の商品にも印刷ができます。</td>				
				</tr>
			</table>
		</div><!-- .about-content4 -->	
	</div><!-- end primary-row -->
	
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>