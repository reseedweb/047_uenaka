<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="company-subpages-title">
			<span class="text1">会社概要</span>
			<span class="text2">Company</span>			
		</h2>
		<h3 class="h3-title">会社概要</h3>	
		<table class="company-table">									
			<tr>
				<th>会社名</th>
				<td>上中製作所</td>									
			</tr>
			<tr>
				<th>所在地</th>
				<td>〒599-8248 大阪府堺市中区深井畑山町177</td>									
			</tr>			
			<tr>
				<th>TEL</th>
				<td>072-279-8798</td>									
			</tr>
			<tr>
				<th>FAX</th>
				<td>072-279-8812</td>									
			</tr>
			<tr>
				<th>代表</th>
				<td>上中 恵仁</td>									
			</tr>
			<tr>
				<th>主な業務内容</th>
				<td>パッド印刷・シルク印刷・レーザーマーク・ホットスタンプ・リベッティング</td>									
			</tr>
		</table>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h3 class="h3-title">アクセス</h3>		
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.0019477977557!2d135.50897456349406!3d34.526562755233!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000da142abab445%3A0x797c5956f8b8c44c!2s177+Fukaihatayamach%C5%8D%2C+Naka-ku%2C+Sakai-shi%2C+%C5%8Csaka-fu+599-8248%2C+Japan!5e0!3m2!1sen!2s!4v1444965788960"  class="companymap" width="750" height="450" frameborder="0" allowfullscreen></iframe>
	</div>
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>