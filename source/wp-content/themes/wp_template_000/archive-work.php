<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="work-subpages-title">
			<span class="text1">制作事例集</span>
			<span class="text2">Works</span>			
		</h2>			
	</div><!-- end primary-row -->

	<?php
	    $work_posts = get_posts( 	array(
			'post_type'=> 'work',
			'posts_per_page' => 3,
			'paged' => get_query_var('paged'),
		));
	?>
	<?php
		$i = 0;	    	    	    
	    foreach($work_posts as $work_post) :  
	?>
	<div class="primary-row clearfix">
		<h3 class="h3-title"><?php echo $work_post->post_title; ?></h3>	
		<div class="work-content clearfix">
			<div class="work-left">
				<?php echo get_the_post_thumbnail($work_post->ID,'medium'); ?>
			</div>
			<div class="work-right">
				<h4 class="work-subtitle">仕様について</h4>
				<table class="work-table">				
					<tr>
						<th>商品名</th>
						<td><?php echo get_field('text1', $work_post->ID); ?></td>
					</tr>
					<tr>
						<th>印刷方法</th>
						<td><?php echo get_field('text2', $work_post->ID); ?></td>
					</tr>
					<tr>
						<th>印刷色数</th>
						<td><?php echo get_field('text3', $work_post->ID); ?></td>
					</tr>	
					<tr>
						<th>オプション</th>
						<td><?php echo get_field('text4', $work_post->ID); ?></td>
					</tr>
				</table>
			</div><!-- .work-right -->		
		</div>		
		<?php $value = get_field( "text5", $work_post->ID);
		if( $value ) {
			echo '<div class="work-info-last">';														
			echo $value;
			echo '</div>';
		} ?>
	</div>
	<?php endforeach;?>
	<div class="primary-row text-center">
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
	<?php wp_reset_query(); ?>
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>