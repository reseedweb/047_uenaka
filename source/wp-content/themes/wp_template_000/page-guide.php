<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="guide-subpages-title">
			<span class="text1">はじめての方へ</span>
			<span class="text2">Guide</span>			
		</h2> 			
		<h3 class="h3-title">パッド印刷.comとは？</h3>
		<p class="guide-top-text">パッド印刷.comでは、主にパッド印刷でのオリジナルグッズ制作を中心に、取り扱い商材のご案内をしております。</p>
		<div class="message-left message-left365 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/guide_content_img1.jpg" alt="はじめての方へ" />		
			</div>
			<div class="text">
				<p>パッド印刷とは凹版を使用して版上のインキをシリコンパッドに一次転写し、被印刷物に二次転写を行なうオフセット印刷の一種です。<br />
				やわらかいパッドが被印刷物になじむ為、平面だけでなく、曲面、凹凸面にも印刷が可能で、ロットに応じて樹脂版と金属版を使いわけます。<br />
				範囲は限られていますが凸凹面に対しても繊細な印刷や、インキの乾燥が早いため、ウェットオンウェットでの連続印刷（多色印刷・重ね打ち）も可能です。</p>
			</div>
		</div><!-- end message-365 -->
		
		<div class="message-right message-right365 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/guide_content_img2.jpg" alt="はじめての方へ" />		
			</div>
			<div class="text">
				<p>パット印刷.comではパッド印刷・縫製のほぼ全ての工程を自社工場内で行うことで、品質を気にされるお客様にも満足いただいております。<br />
				また小ロット・短納期・多品種のご要望にも柔軟に対応させていただきます。<br />
				「製作したいオリジナルグッズがある」「予算や仕様などについて詳しく知りたい」など少しでも興味をお持ちの方は、お問い合わせフォームやお電話からお気軽にご相談ください。</p>
			</div>
		</div><!-- end message-365 -->
		
		<?php get_template_part('part','topsupport'); ?>
		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="message-group message-col238"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->
				<div class="message-col">
					<h3 class="title">グッズの仕様に<br />関するお悩み</h3>
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/guide_box_img1.jpg" alt="はじめての方へ " />
					</div><!-- end image -->    
					<div class="content">
						<p class="text">オリジナルグッズの仕様にお悩みの方、パッド印刷の仕上がりについてもっと知りたい方は、お気軽にご相談ください。<br />
						ご希望があればサンプル品の制作も可能です。<br />
						まずはご要望のイメージやご用途、ご予算などをお伝えください。<br />
						経験豊富な専門スタッフが、お客様のパッド印刷グッズ制作を企画段階からサポートいたします。</p>
						<p class="button"><a href="<?php bloginfo('url'); ?>/support"><img src="<?php bloginfo('template_url'); ?>/img/content/guide_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>
					</div>            
				</div><!-- end message-col -->
												
				<div class="message-col">
					<h3 class="title">納期・ロットに<br />関するお悩み</h3>
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/guide_box_img2.jpg" alt="はじめての方へ " />
					</div><!-- end image -->    
					<div class="content">
						<p class="text">イベントや展示会での使用で納期をお急ぎな案件に関しても、可能な限り短納期にて対応させていただきます。<br />
						500個までの小ロットなら、データ校了後最短5営業日での発送が可能です。<br />
						（※時期によって対応できない場合がありますのでお早めにご相談ください）<br/>
						また、小ロットは１個から制作が可能です。個人のお客様や記念品の制作などでも是非ご活用ください。</p>
						<p class="button"><a href="<?php bloginfo('url'); ?>/support"><img src="<?php bloginfo('template_url'); ?>/img/content/guide_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>
					</div>            
				</div><!-- end message-col --> 
				
				<div class="message-col">
					<h3 class="title">その他のお悩み・<br />ご相談</h3>
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/guide_box_img3.jpg" alt="はじめての方へ " />
					</div><!-- end image -->    
					<div class="content">
						<p class="text">その他デザインデータの作成や、サイトに掲載以外のオリジナルグッズ制作についてのご相談も承っております。<br />
						弊社は長年の知識や経験、自社工場&国内生産に基づく確かな品質に自信があります。<br />
						どんなご相談にも誠意をもってお応えさせていただきますので、是非お気軽にお問い合わせください。</p>
						<p class="button"><a href="<?php bloginfo('url'); ?>/support"><img src="<?php bloginfo('template_url'); ?>/img/content/guide_btn.jpg" alt="<?php bloginfo('name');?>" /></a></p>
					</div>            
				</div><!-- end message-col -->
				         
			</div><!-- end message-row -->
		</div><!-- end message-group -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>