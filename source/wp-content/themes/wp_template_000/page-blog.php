<?php get_header(); ?>                 
	<h2 class="h3-title">スタッフブログ</h2> 
	
    <?php query_posts(array('posts_per_page' => 5, 'paged' => get_query_var('paged') )); ?>
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->
        <div class="primary-row clearfix">            
            <h3 class="price-subtitle"><?php the_title(); ?></h3>            
            <div class="post-row-content clearfix">                
               <!-- <div class="post-row-meta">
                    <i class="fa fa-clock-o"></i><?php the_time('l, F jS, Y'); ?>
                    <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
                    <i class="fa fa-user"></i><span style="color:red;"><?php the_author_link(); ?></span>
                </div>-->
                <div class="post-row-description"><?php the_content(); ?></div>                    
            </div><!-- .post-row-content -->   
        </div>
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>     
		
	<?php get_template_part('part','goods') ;?>
	
<?php get_footer(); ?>