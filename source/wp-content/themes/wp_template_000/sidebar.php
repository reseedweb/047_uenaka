<div class="sidebar-row clearfix"><!-- begin sidebar-row -->    
    <?php get_template_part('part','sideMenu'); ?>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix">
	<p><img src="<?php bloginfo('template_url'); ?>/img/common/side_tel.png" alt="<?php bloginfo('name');?>" /></p>
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/contact">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_contact.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>
	<p class="mt10">
		<a href="<?php bloginfo('url'); ?>/estimate">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_estimate.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix"><!-- begin sidebar-row -->    
    <?php get_template_part('part','sideNav'); ?>
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix">	
	<p>
		<a href="<?php bloginfo('url'); ?>/about">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_padprint.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>	
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/price">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_price.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/work">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_work.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>	
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/support">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_support.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>	
	<p class="mt20">
		<a href="<?php bloginfo('url'); ?>/blog">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_blog.jpg" alt="<?php bloginfo('name');?>" />
		</a>
	</p>	
</div><!-- end sidebar-row -->