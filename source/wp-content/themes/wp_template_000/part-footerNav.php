						<ul class="footer-con-right">
							<li><a href="<?php bloginfo('url'); ?>">トップページ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/guide">はじめての方へ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/about">パッド印刷とは？</a></li>
							<li><a href="<?php bloginfo('url'); ?>/price">参考価格例</a></li>
						</ul>
						<ul class="footer-con-right">
							<li><a href="<?php bloginfo('url'); ?>/work">制作事例集</a></li>
							<li><a href="<?php bloginfo('url'); ?>/flow">ご注文の流れ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/draft">データ入稿について</a></li>
							<li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
						</ul>
						<ul class="footer-con-right">
							<li><a href="<?php bloginfo('url'); ?>/company">会社概要</a></li>
							<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
							<li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/estimate">お見積り</a></li>
						</ul>