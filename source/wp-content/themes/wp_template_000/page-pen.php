<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="pen-subpages-title">
			<span class="text1">ミルキー<br />ボールペン</span>
			<span class="text2">Ballpoint Pen</span>			
		</h2> 	 
		
		<div class="pen-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/pen_slide_img1.jpg" alt="ミルキーボールペン" class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<ul>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/pen_slide_img1.jpg" alt="ミルキーボールペン" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/pen_slide_img2.jpg" alt="ミルキーボールペン" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/pen_slide_img3.jpg" alt="ミルキーボールペン" class="thumb" name="mainImage" /></li>					
				</ul>				
			</div>
		</div><!-- .pen-top-left -->		
		
		<div class="pen-top-right clearfix">
			<h3 class="pen-ttable-title">仕様について</h3>
			<table class="pen-ttable">				
				<tr>
					<th>商品名</th>
					<td>ミルキーボールペン<br />（日本製）</td>					
				</tr>
				<tr>
					<th>サイズ</th>
					<td>約9.5×142(mm)</td>					
				</tr>
				<tr>
					<th>重量</th>
					<td>約9g</td>					
				</tr>
				<tr>
					<th>材質</th>
					<td>PC、ASA、エラストマー</td>					
				</tr>
				<tr>
					<th>カラー</th>
					<td>Pブラック、ブルー、グリーン、ピンク、オレンジ</td>					
				</tr>
				<tr>
					<th>梱包</th>
					<td>OPP袋：無料</td>					
				</tr>
				<tr>
					<th>ロット</th>
					<td>最低ロット：500本～</td>					
				</tr>
				<tr>
					<th>納期</th>
					<td>通常納期：2週間～<br />
						最短納期：5営業日<br />
						（データ校了後発送までの目安です。）
					</td>					
				</tr>
				<tr>
					<th>備考</th>
					<td><span class="icon">■</span> s版代別途3,000円/1色※名入れ専用商品となります。</td>					
				</tr>
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->		

		<div class="pen-top-left mt20 clearfix">
			<p>企業様のノベルティグッズやオリジナルの記念品などに定番の、ミルキーボールペンです。インクから組み立てまで全て日本で製造されており、高品質で滑らかな書き味です。ラバークリップが付いているので長時間の筆記も疲れにくいようになっています。パッド印刷の強みを生かし、立体物への印刷や細かい文字もきれいに印刷できるので、企業様のロゴマークなどきれいに印刷することが可能です。さらに手頃な価格で制作できるので、パッド印刷グッズの中でも大変人気な商品です。</p>
		</div><!-- .pen-top-left -->		
		
		<div class="pen-top-right mt20 clearfix">
			<h3 class="pen-ttable-title">名入れついて</h3>
			<table class="pen-ttable">				
				<tr>
					<th>名入れの<br />サイズ</th>
					<td>ペン軸：W40×H4mm</td>					
				</tr>	
				<tr>
					<th>色数</th>
					<td>パッド印刷：1色、2色<br />フルカラー</td>					
				</tr>					
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->			
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="pen-estimate-btn">
			<a href="<?php bloginfo('url'); ?>/pen#estimate-pen"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="ミルキーボールペン"></a>
		</div>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ミルキーボールペン／参考価格例</h3>
		<p class="pen-content1-text">
			【料金表について】<br />
			・表示価格は税抜き価格となります。<br />
			・表示価格は全商品印刷代込の価格となります。
		</p>
		<table class="pen-content1-table">				
			<tr>
				<th></th>			
				<th>500個</th>			
				<th>1,000個</th>			
				<th>5,000個</th>			
				<th>10,000個</th>			
			</tr>	
			<tr>
				<th>1色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th class="second">2色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
		</table><!-- .pen-content1-table -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">ミルキーボールペン制作のおすすめポイント！</h3>
		<div class="pen-content2 clearfix">
			<div class="pen-con2-left mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 1</div>
					<h4 class="pen-con2-title"><span>立体物への印刷</span>や<span>細かい文字</span>などが<span>きれいに印刷</span>できます！</h4>
				</div>
				<div class="pen-con2-text">
					<p>小さな文字や細かな絵柄を高い再現力を発揮します。<br />
					シルク印刷同様ゴールドもシルバーも一般インクと同料金にてご提供いたします。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 2</div>
					<h4 class="pen-con2-title"><span>名入れの範囲が小さい</span>ので<span>版代も低価格でお得</span>です！</h4>
				</div>
				<div class="pen-con2-text">
					<p>比較的安価な版代なのでノベルティグッズとして提供しやすい商品となっています。</p>
				</div>
			</div><!--.pen-con2-right -->
			
			<div class="pen-con2-left clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 3</div>
					<h4 class="pen-con2-title"><span>手頃な価格</span>で<span>オリジナルグッズ制作</span>ができます！</h4>
				</div>
				<div class="pen-con2-text">
					<p>比較的費用が安い印刷方法ですので、予算が決まっている場合のノベルティ作成ではパッド印刷がおすすめです。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 4</div>
					<h4 class="pen-con2-title"><span>小ロット・短納期のご要望</span>にもお応えします！</h4>
				</div>
				<div class="pen-con2-text">
					<p>最短絵使用日13翌日発送の、ロット数500本からのオーダーが可能です。</p>
				</div>
			</div><!--.pen-con2-right -->

		</div><!-- .pen-content2 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','topwork'); ?>	
	
	<div id="estimate-pen" class="primary-row clearfix"><!-- begin primary-row -->		
		<h3 class="pen-estimate-title"><i class="fa fa-calculator"></i>お見積りフォーム [ミルキーボールペン]</h3>
		<?php echo do_shortcode('[contact-form-7 id="50" title="estimate_pen"]') ?>						
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>