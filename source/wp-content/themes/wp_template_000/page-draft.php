<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="draft-subpages-title">
			<span class="text1">データ入稿について</span>
			<span class="text2">Draft</span>			
		</h2>
		<h3 class="h3-title">【必読】入稿データ作成前に必ずお読みください</h3>
		<div class="draft-content-first clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">デザインの原稿は基本的にIllustrator(ai/eps)形式でお送りください。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">Illustratorをお持ちでない方に限って、PDFのデータでお送りください。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">IllustratorのバージョンはCS4まで対応しております。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">カラーモードはCMYKを選択してください。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">解像度は350dpi以上にしてください。解像度が低いと画像が荒くなる可能性があります。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">Illustratorに配置した画像は全て「eps形式でリンク」もしくは「埋め込み」にしてください。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">フォントは必ず「文字のアウトライン化」をしてください。</p>
		</div>
		
		<div class="draft-content-top clearfix">
			<i class="fa fa-check-circle"></i>
			<p class="draft-info">添付ファイルはできるだけ圧縮して、容量は最大3MBくらいにして頂けるようにお願いします。</p>
		</div>		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">【必読】入稿データ作成前に必ずお読みください</h3>
		<p>作成したデザインデータやイメージ図は下記メールアドレスに添付してご入稿をお願いします。</p>
		<div class="draft-mail"><span><a href="mailto:padprint@gmail.com">padprint@gmail.com</a></span></div>
		<div class="draft-content-end clearfix">
			<div class="draft-left">
				<p class="draft-title">※容量の大きいファイルの場合</p>
				<p class="ln18em"><span class="draft-icon">■</span>4MBを超える容量の大きいデータは極力データを圧縮いただくか、下記データ転送サービスをご利用ください。<br />
					・宅ファイル便（300MBまで無料）<br />
					<a href="http://www.filesend.to/" class="draft-link" target="_blank">http://www.filesend.to/</a><br />
					・ギガファイル便（容量無制限） <br />
					<a href="http://www.gigafile.nu/" class="draft-link" target="_blank">http://www.gigafile.nu/</a>
				</p>
				<p class="ln18em"><span class="draft-icon">■</span>デザインデータをCD-ROMもしくはUSBなどに収容し、下記宛先まで郵送でお送りいただく事も可能です。 尚お送りいただいたCD-ROMなどのご返却出来かねますので、ご了承ください。</p>
			</div>
			<div class="draft-right">
				<p class="draft-title">※郵送される場合</p>
				<p class="pb30 ln18em"><span class="draft-icon">■</span>デザインデータをCD-ROMもしくはUSBなどに収容し、下記宛先まで郵送でお送りいただく事も可能です。 尚お送りいただいたCD-ROMなどのご返却出来かねますので、ご了承ください。</p>
				<p class="draft-title">※データ送付先</p>
				<p class="ln18em">〒5590-0533 大阪府堺市中区深井畑山町177<br />上中製作所 パッド印刷.com 宛</p>
			</div>
		</div><!-- .draft-content-end -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
<?php get_footer(); ?>