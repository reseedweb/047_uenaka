<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h2 class="h2-title">お問い合わせ</h2>
		<div class="contact-form">			
			<?php echo do_shortcode('[contact-form-7 id="42" title="お問い合わせ"]') ?>						
		</div>
	</div>
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>