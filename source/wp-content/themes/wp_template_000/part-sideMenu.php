	<?php if (is_home()) : ?>
        <aside id="side-menu-content">				
			<div class="side-mnu-human"><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_human.png" alt="<?php bloginfo('name');?>" /></div>
			<h2 class="side-mnu-title">商品ラインナップ</h2>
			<ul class="sideMenu">
				<li>
					<a href="<?php bloginfo('url'); ?>/towel">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img1.png" alt="<?php bloginfo('name');?>" /></span>
						ミニタオルハンカチ
					</a>
				</li>        
				<li>
					<a href="<?php bloginfo('url'); ?>/mirror">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img2.png" alt="<?php bloginfo('name');?>" /></span>
						ポケットミラー
					</a>
				</li>   
				<li>
					<a href="<?php bloginfo('url'); ?>/pen">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img3.png" alt="<?php bloginfo('name');?>" /></span>
						ボールペン
					</a>
				</li>   
				<li>
					<a href="<?php bloginfo('url'); ?>/bag">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img4.png" alt="<?php bloginfo('name');?>" /></span>
						トートバッグ
					</a>
				</li>   			                
			</ul>
		</aside>
	<?php else :?>
		<aside id="side-menu-content">							
			<h2 class="side-mnu-title">商品ラインナップ</h2>
			<ul class="sideMenu">
				<li>
					<a href="<?php bloginfo('url'); ?>/towel">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img1.png" alt="<?php bloginfo('name');?>" /></span>
						ミニタオルハンカチ
					</a>
				</li>        
				<li>
					<a href="<?php bloginfo('url'); ?>/mirror">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img2.png" alt="<?php bloginfo('name');?>" /></span>
						ポケットミラー
					</a>
				</li>   
				<li>
					<a href="<?php bloginfo('url'); ?>/pen">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img3.png" alt="<?php bloginfo('name');?>" /></span>
						ボールペン
					</a>
				</li>   
				<li>
					<a href="<?php bloginfo('url'); ?>/bag">
						<span><img src="<?php bloginfo('template_url'); ?>/img/common/side_mnu_img4.png" alt="<?php bloginfo('name');?>" /></span>
						トートバッグ
					</a>
				</li>   			                
			</ul>
		</aside>
	<?php endif;?>		