<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2-title">制作事例</h2> 
    <div id="topslider">
		<div class="topslider-content">
			<?php
				$work_posts = get_posts( 	array(
					'post_type'=> 'work',			
				));
			?>
			<?php		
				foreach($work_posts as $work_post) :  
			?>	
			<div class="slide"><?php echo get_the_post_thumbnail($work_post->ID,'medium'); ?></div>
			<?php endforeach;?>
		</div>		
	</div>
</div><!-- end primary-row -->	


	