<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="towel-subpages-title">
			<span class="text1">今治ミニタオル<br />ハンカチ</span>
			<span class="text2">Mini Towel<br />Handkerchief</span>			
		</h2> 	 
		
		<div class="pen-top-left clearfix">
			<div id="mainalbum">										
				<span><img src="<?php bloginfo('template_url'); ?>/img/content/towel_slide_img1.jpg" alt="今治ミニタオルハンカチ" class="mainImage" /></span>
			</div>
			<div id="subalbum">
				<ul>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/towel_slide_img1.jpg" alt="今治ミニタオルハンカチ" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/towel_slide_img2.jpg" alt="今治ミニタオルハンカチ" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/towel_slide_img3.jpg" alt="今治ミニタオルハンカチ" class="thumb" name="mainImage" /></li>
					<li><img src="<?php bloginfo('template_url'); ?>/img/content/towel_slide_img4.jpg" alt="今治ミニタオルハンカチ" class="thumb" name="mainImage" /></li>
				</ul>				
			</div>
		</div><!-- .pen-top-left -->		 
		
		<div class="pen-top-right clearfix">
			<h3 class="pen-ttable-title">仕様について</h3>
			<table class="pen-ttable">				
				<tr>
					<th>商品名</th>
					<td>今治ミニタオルハンカチ</td>					
				</tr>
				<tr>
					<th>サイズ</th>
					<td>約200×200mm</td>					
				</tr>
				<tr>
					<th>重量</th>
					<td>約14g</td>					
				</tr>
				<tr>
					<th>材質</th>
					<td>綿100％<br />(シャーリング加工）</td>					
				</tr>
				<tr>
					<th>カラー</th>
					<td>藍、若草、檸檬、桜、茜</td>					
				</tr>
				<tr>
					<th>梱包</th>
					<td>OPP袋：無料</td>					
				</tr>
				<tr>
					<th>ロット</th>
					<td>最低ロット：200枚</td>					
				</tr>
				<tr>
					<th>納期</th>
					<td>通常納期：2週間～<br />
					最短納期：5営業日<br />
					(データ校了発送までの<br />期間目安です)
					</td>					
				</tr>
				<tr>
					<th>版代</th>
					<td>店舗販売用・記念品など</td>					
				</tr>
				<tr>
					<th>備考</th>
					<td>パッド印刷：3,000円/1色<br />
						シルク印刷：8,000円/1色</td>					
				</tr>
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->		 

		<div class="pen-top-left mt20 clearfix">
			<p>吸水性と肌触りにこだわり、厳しい審査をクリアした確かな品質の今治タオルミニハンカチです。普段の生活にかかせない商品は、もらって嬉しいノベルティグッズです。実用的で子供から大人まで男女を問わず喜ばれるアイテムですので企業・店舗のノベルティやイベントグッズとしても多用いただけます。</p>
		</div><!-- .pen-top-left -->		
		
		<div class="pen-top-right mt20 clearfix">
			<h3 class="pen-ttable-title">名入れついて</h3>
			<table class="pen-ttable">				
				<tr>
					<th>名入れの<br />サイズ</th>
					<td>W160×H110mm</td>					
				</tr>	
				<tr>
					<th>色数</th>
					<td>パッド印刷：1色、2色<br />
					シルク印刷：1色（広範囲）<br />
					フルカラー</td>					
				</tr>					
			</table><!-- .pen-ttable -->
		</div><!-- .pen-top-right -->		 		
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="pen-estimate-btn">
			<a href="<?php bloginfo('url'); ?>/towel#estimate-towel"><img src="<?php bloginfo('template_url'); ?>/img/content/estimate_btn.jpg" alt="今治ミニタオルハンカチ"></a>
		</div>
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">今治ミニタオルハンカチ／参考価格例</h3>
		<p class="pen-content1-text">
			【料金表について】<br />
			・表示価格は税抜き価格となります。<br />
			・表示価格は全商品印刷代込の価格となります。
		</p>
		<table class="pen-content1-table">				
			<tr>
				<th></th>			
				<th>300個</th>			
				<th>1,000個</th>			
				<th>3,000個</th>			
				<th>5,000個</th>			
			</tr>	
			<tr>
				<th>1色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th class="second">2色</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
			<tr>
				<th>フルカラー</th>
				<td>000円</td>	
				<td>000円</td>
				<td>000円</td>
				<td>000円</td>
			</tr>
		</table><!-- .pen-content1-table -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">今治ミニタオルハンカチ制作のおすすめポイント！</h3>
		<div class="pen-content2 clearfix">
			<div class="pen-con2-left mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 1</div>
					<h4 class="pen-con2-title"><span>立体物への印刷</span>や<span>細かい文字</span>などが<span>きれいに印刷</span>できます！</h4>
				</div>
				<div class="pen-con2-text">
					<p>小さな文字や細かな絵柄を高い再現力を発揮します。<br />
					シルク印刷同様ゴールドもシルバーも一般インクと同料金にてご提供いたします。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right mb20 clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 2</div>
					<h4 class="pen-con2-title"><span>名入れの範囲が小さい</span>ので<span>版代も低価格でお得</span>です！</h4>
				</div>
				<div class="pen-con2-text">
					<p>比較的安価な版代なのでノベルティグッズとして提供しやすい商品となっています。</p>
				</div>
			</div><!--.pen-con2-right -->
			
			<div class="pen-con2-left clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 3</div>
					<h4 class="pen-con2-title"><span>手頃な価格</span>で<span>オリジナルグッズ制作</span>ができます！</h4>
				</div>
				<div class="pen-con2-text">
					<p>比較的費用が安い印刷方法ですので、予算が決まっている場合のノベルティ作成ではパッド印刷がおすすめです。</p>
				</div>
			</div><!--.pen-con2-left -->
			
			<div class="pen-con2-right clearfix">
				<div class="pen-con2-top">
					<div class="pen-con2-step">ポイント 4</div>
					<h4 class="pen-con2-title"><span>小ロット・短納期のご要望</span>にもお応えします！</h4>
				</div>
				<div class="pen-con2-text">
					<p>最短絵使用日13翌日発送の、ロット数500本からのオーダーが可能です。</p>
				</div>
			</div><!--.pen-con2-right -->

		</div><!-- .pen-content2 -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','topwork'); ?>	
	
	<div id="estimate-towel" class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="pen-estimate-title"><i class="fa fa-calculator"></i>お見積りフォーム [今治ミニタオルハンカチ]</h3>
		<?php echo do_shortcode('[contact-form-7 id="113" title="estimate_towel"]') ?>						
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','goods'); ?>	
	
<?php get_footer(); ?>