$(document).ready(function() {		
	/*----- js topnavi current -----*/
	$("#top .top-navi li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("top-navi-current");
        }
	});
	
	/*----- js sideMenu current -----*/
	$("#content .sideMenu li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("sideMenu-current");
        }
	});

	/*----- js sideNav current -----*/
	$("#content .sideNav li a").each(function(){
        if ($(this).attr("href") == window.location.href){
			$(this).addClass("sideNav-current");
        }
	});
	
	/*----- js type-tabs-detail current -----*/
	$("footer .footer-content .footer-con-right li a").each(function(){
       	if ($(this).attr("href") == window.location.href){
		  $(this).addClass('footernav-current');
		}
	});
	
	$('#zip').change(function(){					
		//AjaxZip3.JSONDATA = "https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/zipdata";
		AjaxZip3.zip2addr(this,'','pref','addr1','addr2');
	});
	
	$('#content .slider').slick({
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,
	});
	
	$('#topslider .topslider-content').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		centerPadding: '20px',
		slidesToShow: 4,
		slidesToScroll: 4
	});
	
});


jQuery(function(){
	$(this).tgImageChange({
	selectorThumb : ".thumb", // ←★サムネイル画像セレクタ(ドットは必須)
	fadeOutTime : 50, // ←★カーソルON時のアニメーション時間
	fadeInTime : 200, // ←★カーソルOFF時のアニメーション時間
	thumbCssBorder : '2px solid #ff5a71', // ←★カーソルON時のサムネイル枠CSS
	});
}); 
