;(function($){
    $.fn.tgImageChange = function(options){

        var opts = $.extend({}, $.fn.tgImageChange.defaults, options);

        $(opts.selectorThumb).mouseover(function(){

            // "_thumb"ã‚’å‰Šã£ãŸç”»åƒåå–å¾—
            var selectedSrc = $(this).attr('src').replace(/^(.+)_thumb(\.gif|\.jpg|\.png+)$/, "$1"+"$2");

            // nameå–å¾—
            var selectedName = $(this).attr('name');

            // é–¢é€£ä»˜ã‘ã•ã‚Œã¦ã„ã‚‹ãƒ¡ã‚¤ãƒ³ç”»åƒå…¥ã‚Œæ›¿ãˆ
            $('img.'+selectedName).stop().fadeOut(opts.fadeOutTime,
                function(){
                    $('img.'+selectedName).attr('src', selectedSrc).stop().fadeIn(opts.fadeInTime);
                }
            );

            // ã‚µãƒ ãƒã‚¤ãƒ«ã®æž ã‚’å¤‰æ›´
            $(this).css({"border":opts.thumbCssBorder});
        });

        // ãƒžã‚¦ã‚¹ã‚¢ã‚¦ãƒˆã§ã‚µãƒ ãƒã‚¤ãƒ«æž ã‚’å…ƒã«æˆ»ã™
        $(opts.selectorThumb).mouseout(function(){
            $(this).css({"border":""});
        });
    }
})(jQuery);