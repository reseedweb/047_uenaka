<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="slider">
		<div><img src="<?php bloginfo('template_url'); ?>/img/top/slider_img1.jpg" alt="<?php bloginfo('name');?>" /></div>
		<div><img src="<?php bloginfo('template_url'); ?>/img/top/slider_img2.jpg" alt="<?php bloginfo('name');?>" /></div>
		<div><img src="<?php bloginfo('template_url'); ?>/img/top/slider_img3.jpg" alt="<?php bloginfo('name');?>" /></div>
	</div>
</div><!-- end primary-row -->		


<div id="top-content"><!-- begin primary-row -->
	<h2 class="h2-title">特集コンテンツ</h2> 
    <div class="top-content1">
		<div class="top-content1-btn"><a href="<?php bloginfo('url'); ?>/about"><img src="<?php bloginfo('template_url'); ?>/img/top/top_content12_btn.png" alt="<?php bloginfo('name');?>" /></a></div>
	</div>
	
	<?php get_template_part('part','topsupport'); ?> 
</div><!-- end primary-row -->	


<?php get_template_part('part','goods'); ?>  			

<?php get_template_part('part','topwork'); ?>  			

<?php get_footer(); ?>